import React from "react";
import './App.css';
import gumbaz from './image/the little towers.png';
import mosque from './image/mosque.png';
import cloud from './image/the clouds.png';
import moon from './image/the moon.png';
import tower from './image/towers.png';

function App() {

    return (
        <div className="App">
            <div className="container">
                <header>
                    <button className="letsGo">Let's go</button>
                    <a href="#" className="about">About Us</a>
                </header>
                <footer>
                    <button className="batafsil">Batafsil</button>
                </footer>

                <div className="images">
                    <img src={mosque} alt="" id="mosque"/>
                    <img src={tower} alt="" id="tower"/>
                </div>
                <img src={gumbaz} alt="" id="gumbaz"/>
                <img src={moon} alt="" id="moon"/>
                <img src={cloud} alt="" id="cloud"/>

            </div>
        </div>
    );
}

export default App;