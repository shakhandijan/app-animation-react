import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import './Batafsil.css'

class Batafsil extends Component {
    render() {
        return (
            <div className="card">
                <span>Clicked</span>
                <br/>
                <p className="lorem">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores placeat
                    reiciendis voluptatibus? Adipisci delectus, doloribus ducimus eius et eum explicabo facilis
                    fugiat hic illum in incidunt ipsa iure laudantium maxime mollitia, nisi nobis obcaecati
                    officia qui repellat reprehenderit rerum sed similique sit voluptate voluptatum! Aperiam
                    blanditiis commodi dolorum eaque est ex impedit obcaecati sequi soluta totam! Accusamus
                    eligendi labore libero?
                    <br/>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam aliquid architecto beatae
                    corporis culpa cupiditate dicta esse est exercitationem, ipsa laborum minima minus nemo nihil
                    obcaecati placeat quaerat quasi quibusdam quis recusandae repellat reprehenderit sapiente suscipit
                    totam vitae voluptatem voluptatum. Accusamus assumenda ducimus explicabo harum inventore ipsa labore
                    magnam, obcaecati, officiis optio perferendis rerum ut vel voluptates voluptatibus? Id.
                    <br/>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A consequatur culpa fugiat, ipsum minima
                    quia. Aperiam deserunt harum ipsam, maiores optio ratione reprehenderit? Et expedita similique
                    tempora! Deserunt eius esse facere illo impedit incidunt inventore libero, magnam magni nam
                    necessitatibus nihil pariatur quo reiciendis rem saepe sequi tempore veritatis voluptate.
                </p>
            </div>
        );
    }
}

export default Batafsil;